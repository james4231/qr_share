# qr_share

Tools that start a python http server to serve files from a folder, and generate a QR code of the URL to that server for quick access from a mobile device.

## Requirements

### python3

Set as default environment. Scripts can be adjusted to work with python2.7 by using SimpleHTTPServer module instead of http.server and removing the --bind argument.

### [qrcode 6.0](https://pypi.org/project/qrcode/)
```
pip install qrcode
```

## Folders

### automator

* Mac OS Automator service that will start the http server and generate a qr code to the URL.
* Download the folder and open the .workflow file with Automator, and install as a service.
* Right click on a folder, Select Services, and Simple HTTP Server to run the service.
* Note: assumes WiFi interface (typically en0) will be used to access server. Adjust as necessary.